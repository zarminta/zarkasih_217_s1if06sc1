<?php

require_once "koneksi.php";
require_once "create_message.php";

$query = "DELETE FROM mahasiswa;";
if ($conn->query($query) === TRUE) {
    echo "Success";
    $conn->close();
    create_message("Hapus Data Berhasil","success","check");
    $files = glob('uploads/*'); // get all file names
    foreach($files as $file){ // iterate files
        if(is_file($file)) {
            unlink($file); // delete file
        }
    }
    header("location:index.php");
    exit();
} else {
    echo "Error: " . $query . "<br>" . $conn->error;
    $conn->close();
    exit();
}

