<?php

require_once "koneksi.php";
require_once "create_message.php";
//require_once "upload_process.php";

if(isset($_POST['mahasiswa_id'])){
    //Kondisi Update
    $sql = "UPDATE mahasiswa SET nama_lengkap = '".$_POST['nama_lengkap']."',alamat = '".$_POST['alamat']."',kelas_id = '".$_POST['kelas_id']."' WHERE (`mahasiswa_id` = '".$_POST['mahasiswa_id']."');";
    if ($conn->query($sql) === TRUE) {
        $conn->close();
        create_message("Ubah Data Berhasil","success","check");
        header("location:".$_SERVER['HTTP_REFERER']);
        exit();
    } else {
        $conn->close();
        create_message("Error: " . $sql . "<br>" . $conn->error,"danger","warning");
        header("location:".$_SERVER['HTTP_REFERER']);
        exit();
    }
}
else{
    //Kondisi Insert
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["foto"]["name"]);
    $error = false;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $sql = "INSERT INTO mahasiswa (nama_lengkap, kelas_id, alamat, foto) VALUES ('".$_POST['nama_lengkap']."', ".$_POST['kelas_id'].", '".$_POST['alamat']."', '".$target_file."')";

    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["foto"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $error = false;
        } else {
            echo "File is not an image.";
            $error = false;
        }
    }

    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $error = true;
    }

    if ($_FILES["foto"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $error = true;
    }

    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $error = true;
    }

    if ($error == true) {
        echo "Sorry, your file was not uploaded."; } else {
        if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["foto"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
//    $sql = "INSERT INTO mahasiswa (nama_lengkap, kelas_id, alamat) VALUES ('".$_POST['nama_lengkap']."', ".$_POST['kelas_id'].", '".$_POST['alamat']."')";

    if ($conn->query($sql) === TRUE) {
        $conn->close();
        header("location:index.php");
        exit();
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
        $conn->close();
        exit();
    }

}
