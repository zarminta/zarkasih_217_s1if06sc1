
<?php

require_once "koneksi.php";
session_start();
$qkelas = "select * from kelas";
$data_kelas = $conn->query($qkelas);
$qmahasiswa = "select * from mahasiswa";
$data_mahasiswa = $conn->query($qmahasiswa);

$query_total = "SELECT COUNT(*) FROM mahasiswa;";
$query_result = $conn->query($query_total);
$count = mysqli_fetch_assoc($query_result);

$qselect_mahasiswa = "select * from mahasiswa left join kelas on kelas.kelas_id = mahasiswa.kelas_id where mahasiswa_id = ".$_GET['mahasiswa_id'];
foreach($conn->query($qselect_mahasiswa) as $value){
    $data_select_mahasiswa=$value;
} ;

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>Form Mahasiswa</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
<div class="container">
    <div class="py-5 text-center">
        <h2>Form Mahasiswa</h2>
    </div>

    <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
            <h4 class="d-flex justify-content-between align-items-center mb-3">
                <span class="text-muted">Data Mahasiswa</span>
                <span class="badge badge-secondary badge-pill"><?php foreach ($count as $total){ echo $total;}?></span>
                <button type="button" href="delete.php" name="delete" value="delete" class="btn btn-danger btn-sm">Delete All</button>
            </h4>
            <?php
            foreach($data_mahasiswa as $index => $value){
            ?>
            <ul class="list-group mb-3">
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <img src="<?php echo $value['foto'] ?>" width="50" height="50" alt="foto">
                    <div>
                        <h6 class="my-0"><?php echo $value['nama_lengkap'] ?></h6>
                        <small class="text-muted"><?php echo $value['alamat'] ?></small>
                    </div>
                    <span class="text-muted">
                        <?php
                        $query = "SELECT nama FROM kelas INNER JOIN mahasiswa ON kelas.kelas_id = mahasiswa.kelas_id WHERE kelas.kelas_id=" . $value["kelas_id"] . ";";
                        $data_kelas_mhs = $conn->query($query);
                        while($obj = mysqli_fetch_assoc($data_kelas_mhs)) {
                            echo $obj['nama'];
                        }?></span>
                    <a href="update_form.php?mahasiswa_id=<?php echo $value['mahasiswa_id'] ?>" type="button" class="close"><span class="fa fa-pencil"></span></a>
                    <a href="hapus_data.php?mahasiswa_id=<?php echo $value['mahasiswa_id'] ?>" type="button" class="close"><span class="fa fa-trash"></span></a>
                </li>
            </ul>
            <?php } ?>
        </div>
        <div class="col-md-8 order-md-1">
            <h4 class="mb-3">Input Data</h4>
            <?php include "read_message.php" ?>
            <form action ="simpan_mahasiswa.php" method="POST" >
                <input type="hidden" name="mahasiswa_id" value="<?php echo $data_select_mahasiswa['mahasiswa_id'] ?>">
                <div class="mb-3">
                    <label for="nama_lengkap" value="<?php echo $data_select_mahasiswa['nama_lengkap'] ?>" >Nama Lengkap</label>
                    <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" required>
                </div>
                <div class="mb-3">
                    <label for="alamat" value="<?php echo $data_select_mahasiswa['alamat'] ?>">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" required>
                </div>
                <div class="mb-3">
                    <label for="kelas">Kelas</label>
                    <select class="custom-select d-block w-100" id="Kelas" name="kelas_id" required>
                        <option value="">Pilih...</option>
                        <?php
                            foreach ($data_kelas as $index => $value){
                                if($data_select_mahasiswa['kelas_id']==$value['kelas_id']){
                                    $is_selected = 'selected';
                                }
                                else{
                                    $is_selected = '';
                                }
                        ?>
                        <option <?php echo $is_selected ?> value="<?php echo $value['kelas_id'] ?>"><?php echo $value['nama'] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="row">
                </div>
                <hr class="mb-4">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Update Data</button>
                <a href="index.php" class="btn btn-warning btn-lg btn-block" type="submit">Batal</a>
            </form>
        </div>
    </div>

</div>
<footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">&copy; 2017-2019 Company Name</p>
    <ul class="list-inline">
        <li class="list-inline-item"><a href="#">Privacy</a></li>
        <li class="list-inline-item"><a href="#">Terms</a></li>
        <li class="list-inline-item"><a href="#">Support</a></li>
    </ul>
</footer>
</div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
-->
</body>
</html>