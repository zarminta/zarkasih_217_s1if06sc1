<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    public function create()
    {
    $data['module']['name'] = "Tambah Mahasiswa";
    return view('adminlte.student.create',['data' => $data]);
    }

}
