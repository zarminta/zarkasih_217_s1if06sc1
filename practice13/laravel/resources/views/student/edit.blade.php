<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>Edit Mahasiswa</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-xl-6">
            <h1>Edit Mahasiswa</h1>
            <hr>
            <form action="{{ route('student.update',['student' => $student->id]) }}" method="POST" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="nim">NIM</label>
                <input type="text"
                       class="form-control @error('nim') is-invalid @enderror"
                       id="nim" name="nim" value="{{ old('nim') ?? $student->nim }}">
                @error('nim')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama">Nama Lengkap</label>
                <input type="text"
                       class="form-control @error('name') is-invalid @enderror"
                       id="nama" name="nama" value="{{ old('name') ?? $student->name }}">
                @error('name')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Jenis Kelamin</label>
                <div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="jenis_kelamin"
                               id="laki_laki" value="L"
                            {{ (old('gender') ?? $student->gender)
                            == 'L' ? 'checked': '' }} >
                        <label class="form-check-label" for="laki_laki">Laki-laki</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="jenis_kelamin"
                               id="perempuan" value="P"
                            {{ (old('gender') ?? $student->gender)
                            == 'P' ? 'checked': '' }} >
                        <label class="form-check-label" for="perempuan">Perempuan</label>
                    </div>
                    @error('gender')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label for="jurusan">Jurusan</label>
                <select class="form-control" name="jurusan" id="jurusan">
                    <option value="Teknik Informatika"
                        {{ (old('departement') ?? $student->departement)==
                        'Teknik Informatika' ? 'selected': '' }} > Teknik Informatika
                    </option>
                    <option value="Sistem Informasi"
                        {{ (old('departement') ?? $student->departement)==
                        'Sistem Informasi' ? 'selected': '' }} > Sistem Informasi
                    </option>
                    <option value="Ilmu Komputer"
                        {{ (old('departement') ?? $student->departement)==
                        'Ilmu Komputer' ? 'selected': '' }} > Ilmu Komputer
                    </option>
                    <option value="Teknik Komputer"
                        {{ (old('departement') ?? $student->departement)==
                        'Teknik Komputer' ? 'selected': '' }} > Teknik Komputer
                    </option>
                    <option value="Teknik Telekomunikasi"
                        {{(old('departement')??$student->departement)==
                        'Teknik Telekomunikasi'?'selected':''}} > Teknik Telekomunikasi
                    </option>
                </select>
                @error('departement')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea class="form-control" id="alamat" rows="3"
                name="alamat">{{ old('alamat') ?? $student->address}}</textarea>
            </div>
            <div class="form-group">
                    <label for="image">Gambar Profile</label>
                    <input type="file" class="form-control-file" id="image" name="image">
                    @error('image')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary mb-2">Update</button>
            </form>
        </div>
    </div>
</div>
</div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
-->
</body>
</html>