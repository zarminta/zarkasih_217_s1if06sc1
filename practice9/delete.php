<?php

require_once "koneksi.php";

$query = "DELETE FROM mahasiswa;";
if ($conn->query($query) === TRUE) {
    echo "Success Menghapus Data Anda";
    $conn->close();
    exit();
} else {
    echo "Error: " . $query . "<br>" . $conn->error;
    $conn->close();
    exit();
}